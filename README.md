This project has been migrated to Eclipse Foundation, and it can be found under https://gitlab.eclipse.org/eclipse/xfsc/

## Install dependencies

    yarn

## Run

    npm start

## Build

    DISABLE_ESLINT_PLUGIN=true npm run build

## Check and format all files with pretier

    yarn prettier --write .
